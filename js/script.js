console.log("belajar javascript bersama sanbercode");

var buttonPushed = 1;
switch (buttonPushed) {
  case 1: {
    console.log("matikan TV!");
    break;
  }
  case 2: {
    console.log("turunkan volume TV!");
    break;
  }
  case 3: {
    console.log("tingkatkan volume TV!");
    break;
  }
  case 4: {
    console.log("matikan suara TV!");
    break;
  }
  default: {
    console.log("Tidak terjadi apa-apa");
  }
}

function Book(type, author) {
  this.type = type;
  this.author = author;
  this.getDetails = function () {
    return this.type + " written by " + this.author;
  };
}

var book = new Book("Fiction", "Peter King");

console.log(book.getDetails());

console.log("Start");

setTimeout(() => {
  console.log("In timeout");
}, 1000); // Wait 1s to run

console.log("End");
